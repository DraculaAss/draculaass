// DracView.c ... DracView ADT implementation
#ifndef ROUND_SIZE
#define ROUND_SIZE 40
#endif

#ifndef TURN_SIZE
#define TURN_SIZE 8
#endif

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "Globals.h"
#include "Game.h"
#include "GameView.h"
#include "DracView.h"

#include <stdio.h>
// #include "Map.h" ... if you decide to use the Map ADT
     
struct dracView {
    GameView gameView;
};

struct gameView {
    Round          currRound;
    PlayerID       currPlayer;
    int            currScore;
    int            currHealth[NUM_PLAYERS];
    char           *currPastPlays;
    PlayerMessage  currMessages[];
};
     

// Creates a new DracView to summarise the current state of the game
DracView newDracView(char *pastPlays, PlayerMessage messages[])
{
    //REPLACE THIS WITH YOUR OWN IMPLEMENTATION
    DracView dracView = malloc(sizeof(struct dracView));
    dracView->gameView = newGameView(pastPlays, messages);
    //dracView->hello = 42;
    return dracView;
}
     
     
// Frees all memory previously allocated for the DracView toBeDeleted
void disposeDracView(DracView toBeDeleted)
{
    disposeGameView(toBeDeleted->gameView);
    free( toBeDeleted );
}


//// Functions to return simple information about the current state of the game

// Get the current round - COMPLETE
Round giveMeTheRound(DracView currentView)
{
    return getRound(currentView->gameView);
}

// Get the current score - COMPLETE
int giveMeTheScore(DracView currentView)
{
    return getScore(currentView->gameView);;
}

// Get the current health points for a given player - COMPLETE
int howHealthyIs(DracView currentView, PlayerID player)
{
    return getHealth(currentView->gameView, player);
}

// Get the current location id of a given player - COMPLETE
LocationID whereIs(DracView currentView, PlayerID player)
{
    return getLocation(currentView->gameView, player); 
}

// Get the most recent move of a given player - COMPLETE
void lastMove(DracView currentView, PlayerID player,
                 LocationID *start, LocationID *end)
{
    LocationID history[TRAIL_SIZE];
    getHistory(currentView->gameView, player, history);
    start[0] = history[1];
    end[0] = history[0];
    return;
}

// Find out what minions are placed at the specified location - COMPLETE
void whatsThere(DracView currentView, LocationID where,
                         int *numTraps, int *numVamps)
{
    int traps = 0;
    int vamps = 0;

    //LocationID history[TRAIL_SIZE];
    // getHistory(currentView->gameView, PLAYER_DRACULA, history);

    if (isLand(where) == TRUE)
    {
        /* 
        ###########
        #ATTEMPT 1#
        ###########

        int i;
        for (i = 0; i < TRAIL_SIZE; i++)
        {
            if (history[i] == where)
            {
                if (currentView->gameView->currRound % 13 == 0)
                {
                    (*numVamps)++;
                }
                else if (*numTraps < 3)
                {
                    (*numTraps)++;
                }
            }
        }
        */

        /*
        ###########
        #ATTEMPT 2#
        ###########
        */

        int i = 0;
        int j;
        char currLocation[2];
        char whereStr[2];
        sprintf(whereStr, "%d", where);

        while (i <= currentView->gameView->currRound)
        {
            j = 0;
            while (j < 4) //look at each of the hunters' moves
            {
                //store location in currLocation
                currLocation[0] = currentView->gameView->currPastPlays[i*ROUND_SIZE+j*TURN_SIZE+1];
                currLocation[1] = currentView->gameView->currPastPlays[i*ROUND_SIZE+j*TURN_SIZE+2];

                //subtract if hunter encounters trap or vamp
                if (currentView->gameView->currPastPlays[i*ROUND_SIZE+j*TURN_SIZE+3] == 'T'
                    && currLocation == whereStr)
                {
                    traps--;
                }
                if (currentView->gameView->currPastPlays[i*ROUND_SIZE+j*TURN_SIZE+4] == 'V'
                    && currLocation == whereStr)
                {
                    printf("-\n");
                    vamps--;
                }
                j++;
            }
            printf("ID %d, where %s\n", *currLocation, whereStr);
            //add if dracula places trap or vamp
            if (currentView->gameView->currPastPlays[i*ROUND_SIZE+j*TURN_SIZE+3] == 'T'
                && currLocation == whereStr)
            {
                traps++;
            }
            if (currentView->gameView->currPastPlays[i*ROUND_SIZE+j*TURN_SIZE+4] == 'V'
                && currLocation == whereStr)
            {
                printf("+\n");
                vamps++;
            } 
            //trap or vamp has left the train
            if (currentView->gameView->currPastPlays[i*ROUND_SIZE+j*TURN_SIZE+5] == 'M'
                && currLocation == whereStr)
            {
                traps--;
            }
            else if (currentView->gameView->currPastPlays[i*ROUND_SIZE+j*TURN_SIZE+5] == 'V'
                && currLocation == whereStr)
            {
                printf("-\n");
                vamps--;
            } 
            i++;
        } 

        /*
        int i = 0;
        while (i < currentView->gameView->currRound*5+currentView->gameView->currPlayer)
        {
            if (currentView->currPastPlays[i*TURN_SIZE] == 'D')
            {
                if (currentView->currPastPlays[i*TURN_SIZE+1] == 'D')
            }
            i++
        }
        */
    }
    numTraps = &traps;
    numVamps = &vamps;
    printf("%d, %d\n", *numTraps, *numVamps);
    return;
}

//// Functions that return information about the history of the game

// Fills the trail array with the location ids of the last 6 turns - COMPLETE
void giveMeTheTrail(DracView currentView, PlayerID player,
                            LocationID trail[TRAIL_SIZE])
{
    getHistory(currentView->gameView, player, trail);
}

//// Functions that query the map to find information about connectivity

// What are my (Dracula's) possible next moves (locations) - COMPLETE
LocationID *whereCanIgo(DracView currentView, int *numLocations, int road, int sea)
{
    int rail = FALSE;

    LocationID src = getLocation(currentView->gameView, PLAYER_DRACULA);

    return connectedLocations(currentView->gameView, numLocations, src, 
                                PLAYER_DRACULA, currentView->gameView->currRound, road, rail, sea);
}

// What are the specified player's next possible moves - COMPLETE
LocationID *whereCanTheyGo(DracView currentView, int *numLocations,
                           PlayerID player, int road, int rail, int sea)
{
    LocationID from = getLocation(currentView->gameView, currentView->gameView->currPlayer);

    return connectedLocations(currentView->gameView, numLocations, from, 
                                player, currentView->gameView->currRound, road, rail, sea);
}
