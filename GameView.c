// GameView.c ... GameView ADT implementation

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "Globals.h"
#include "Game.h"
#include "GameView.h"
//#include "Map.h"

#include <stdio.h>

//from Map.c -------------------------------------------------------------------------------------------

// Map.c ... implementation of Map type
// (a specialised version of the Map ADT)
// You can change this as much as you want

//from map.h
//typedef struct MapRep *Map; 




typedef struct MapRep *Map; 

typedef struct vNode *VList;

Map  newMap();  
void disposeMap(Map g); 
void showMap(Map g); 
//int  numV(Map g);
//int  numE(Map g, TransportID t);

typedef struct edge{
    LocationID  start;
    LocationID  end;
    TransportID type;
} Edge;

struct vNode {
   LocationID  v;    // ALICANTE, etc
   TransportID type; // ROAD, RAIL, BOAT
   VList       next; // link to next node
};

struct MapRep {
   int   nV;         // #vertices
   int   nE;         // #edges
   VList connections[NUM_MAP_LOCATIONS]; // array of lists
};

static void addConnections(Map);

// Create a new empty graph (for a map)
// #Vertices always same as NUM_PLACES
Map newMap()
{
   int i;
   Map g = malloc(sizeof(struct MapRep));
   assert(g != NULL);
   g->nV = NUM_MAP_LOCATIONS;
   for (i = 0; i < g->nV; i++){
      g->connections[i] = NULL;
   }
   g->nE = 0;
   addConnections(g);
   return g;
}

// Remove an existing graph
void disposeMap(Map g)
{
   int i;
   VList curr;
   VList next;
   assert(g != NULL);
   assert(g->connections != NULL);

   for (i = 0; i < g->nV; i++){
       curr = g->connections[i];
       while(curr != NULL){
          next = curr->next;
          free(curr);
          curr=next;
       }
   }
   free(g);
}

static VList insertVList(VList L, LocationID v, TransportID type)
{
   VList newV = malloc(sizeof(struct vNode));
   newV->v = v;
   newV->type = type;
   newV->next = L;
   return newV;
}

static int inVList(VList L, LocationID v, TransportID type)
{
    VList cur;
    for (cur = L; cur != NULL; cur = cur->next) {
        if (cur->v == v && cur->type == type) return 1;
    }
    return 0;
}

// Add a new edge to the Map/Graph
void addLink(Map g, LocationID start, LocationID end, TransportID type)
{
    assert(g != NULL);
    // don't add edges twice
    if (!inVList(g->connections[start],end,type)) {
    g->connections[start] = insertVList(g->connections[start],end,type);
    g->connections[end] = insertVList(g->connections[end],start,type);
    g->nE++;
    }
}

// Display content of Map/Graph
void showMap(Map g)
{
   assert(g != NULL);
   printf("V=%d, E=%d\n", g->nV, g->nE);
   int i;
   for (i = 0; i < g->nV; i++) {
      VList n = g->connections[i];
      while (n != NULL) {
         printf("%s connects to %s ",idToName(i),idToName(n->v));
         switch (n->type) {
         case ROAD: printf("by road\n"); break;
         case RAIL: printf("by rail\n"); break;
         case BOAT: printf("by boat\n"); break;
         default:   printf("by ????\n"); break;
         }
         n = n->next;
      }
   }
}

// Return count of nodes
int numV(Map g)
{
   assert(g != NULL);
   return g->nV;
}

// Return count of edges of a particular type
int numE(Map g, TransportID type)
{
   int i, nE=0;
   assert(g != NULL);
   assert(type >= 0 && type <= ANY);
   for (i = 0; i < g->nV; i++) {
      VList n = g->connections[i];
      while (n != NULL) {
         if (n->type == type || type == ANY) nE++;
         n = n->next;
      }
    }
    return nE;
}

// Add edges to Graph representing map of Europe
static void addConnections(Map g)
{
   //### ROAD Connections ###

   addLink(g, ALICANTE, GRANADA, ROAD);
   addLink(g, ALICANTE, MADRID, ROAD);
   addLink(g, ALICANTE, SARAGOSSA, ROAD);
   addLink(g, AMSTERDAM, BRUSSELS, ROAD);
   addLink(g, AMSTERDAM, COLOGNE, ROAD);
   addLink(g, ATHENS, VALONA, ROAD);
   addLink(g, BARCELONA, SARAGOSSA, ROAD);
   addLink(g, BARCELONA, TOULOUSE, ROAD);
   addLink(g, BARI, NAPLES, ROAD);
   addLink(g, BARI, ROME, ROAD);
   addLink(g, BELGRADE, BUCHAREST, ROAD);
   addLink(g, BELGRADE, KLAUSENBURG, ROAD);
   addLink(g, BELGRADE, SARAJEVO, ROAD);
   addLink(g, BELGRADE, SOFIA, ROAD);
   addLink(g, BELGRADE, ST_JOSEPH_AND_ST_MARYS, ROAD);
   addLink(g, BELGRADE, SZEGED, ROAD);
   addLink(g, BERLIN, HAMBURG, ROAD);
   addLink(g, BERLIN, LEIPZIG, ROAD);
   addLink(g, BERLIN, PRAGUE, ROAD);
   addLink(g, BORDEAUX, CLERMONT_FERRAND, ROAD);
   addLink(g, BORDEAUX, NANTES, ROAD);
   addLink(g, BORDEAUX, SARAGOSSA, ROAD);
   addLink(g, BORDEAUX, TOULOUSE, ROAD);
   addLink(g, BRUSSELS, COLOGNE, ROAD);
   addLink(g, BRUSSELS, LE_HAVRE, ROAD);
   addLink(g, BRUSSELS, PARIS, ROAD);
   addLink(g, BRUSSELS, STRASBOURG, ROAD);
   addLink(g, BUCHAREST, CONSTANTA, ROAD);
   addLink(g, BUCHAREST, GALATZ, ROAD);
   addLink(g, BUCHAREST, KLAUSENBURG, ROAD);
   addLink(g, BUCHAREST, SOFIA, ROAD);
   addLink(g, BUDAPEST, KLAUSENBURG, ROAD);
   addLink(g, BUDAPEST, SZEGED, ROAD);
   addLink(g, BUDAPEST, VIENNA, ROAD);
   addLink(g, BUDAPEST, ZAGREB, ROAD);
   addLink(g, CADIZ, GRANADA, ROAD);
   addLink(g, CADIZ, LISBON, ROAD);
   addLink(g, CADIZ, MADRID, ROAD);
   addLink(g, CASTLE_DRACULA, GALATZ, ROAD);
   addLink(g, CASTLE_DRACULA, KLAUSENBURG, ROAD);
   addLink(g, CLERMONT_FERRAND, GENEVA, ROAD);
   addLink(g, CLERMONT_FERRAND, MARSEILLES, ROAD);
   addLink(g, CLERMONT_FERRAND, NANTES, ROAD);
   addLink(g, CLERMONT_FERRAND, PARIS, ROAD);
   addLink(g, CLERMONT_FERRAND, TOULOUSE, ROAD);
   addLink(g, COLOGNE, FRANKFURT, ROAD);
   addLink(g, COLOGNE, HAMBURG, ROAD);
   addLink(g, COLOGNE, LEIPZIG, ROAD);
   addLink(g, COLOGNE, STRASBOURG, ROAD);
   addLink(g, CONSTANTA, GALATZ, ROAD);
   addLink(g, CONSTANTA, VARNA, ROAD);
   addLink(g, DUBLIN, GALWAY, ROAD);
   addLink(g, EDINBURGH, MANCHESTER, ROAD);
   addLink(g, FLORENCE, GENOA, ROAD);
   addLink(g, FLORENCE, ROME, ROAD);
   addLink(g, FLORENCE, VENICE, ROAD);
   addLink(g, FRANKFURT, LEIPZIG, ROAD);
   addLink(g, FRANKFURT, NUREMBURG, ROAD);
   addLink(g, FRANKFURT, STRASBOURG, ROAD);
   addLink(g, GALATZ, KLAUSENBURG, ROAD);
   addLink(g, GENEVA, MARSEILLES, ROAD);
   addLink(g, GENEVA, PARIS, ROAD);
   addLink(g, GENEVA, STRASBOURG, ROAD);
   addLink(g, GENEVA, ZURICH, ROAD);
   addLink(g, GENOA, MARSEILLES, ROAD);
   addLink(g, GENOA, MILAN, ROAD);
   addLink(g, GENOA, VENICE, ROAD);
   addLink(g, GRANADA, MADRID, ROAD);
   addLink(g, HAMBURG, LEIPZIG, ROAD);
   addLink(g, KLAUSENBURG, SZEGED, ROAD);
   addLink(g, LEIPZIG, NUREMBURG, ROAD);
   addLink(g, LE_HAVRE, NANTES, ROAD);
   addLink(g, LE_HAVRE, PARIS, ROAD);
   addLink(g, LISBON, MADRID, ROAD);
   addLink(g, LISBON, SANTANDER, ROAD);
   addLink(g, LIVERPOOL, MANCHESTER, ROAD);
   addLink(g, LIVERPOOL, SWANSEA, ROAD);
   addLink(g, LONDON, MANCHESTER, ROAD);
   addLink(g, LONDON, PLYMOUTH, ROAD);
   addLink(g, LONDON, SWANSEA, ROAD);
   addLink(g, MADRID, SANTANDER, ROAD);
   addLink(g, MADRID, SARAGOSSA, ROAD);
   addLink(g, MARSEILLES, MILAN, ROAD);
   addLink(g, MARSEILLES, TOULOUSE, ROAD);
   addLink(g, MARSEILLES, ZURICH, ROAD);
   addLink(g, MILAN, MUNICH, ROAD);
   addLink(g, MILAN, VENICE, ROAD);
   addLink(g, MILAN, ZURICH, ROAD);
   addLink(g, MUNICH, NUREMBURG, ROAD);
   addLink(g, MUNICH, STRASBOURG, ROAD);
   addLink(g, MUNICH, VENICE, ROAD);
   addLink(g, MUNICH, VIENNA, ROAD);
   addLink(g, MUNICH, ZAGREB, ROAD);
   addLink(g, MUNICH, ZURICH, ROAD);
   addLink(g, NANTES, PARIS, ROAD);
   addLink(g, NAPLES, ROME, ROAD);
   addLink(g, NUREMBURG, PRAGUE, ROAD);
   addLink(g, NUREMBURG, STRASBOURG, ROAD);
   addLink(g, PARIS, STRASBOURG, ROAD);
   addLink(g, PRAGUE, VIENNA, ROAD);
   addLink(g, SALONICA, SOFIA, ROAD);
   addLink(g, SALONICA, VALONA, ROAD);
   addLink(g, SANTANDER, SARAGOSSA, ROAD);
   addLink(g, SARAGOSSA, TOULOUSE, ROAD);
   addLink(g, SARAJEVO, SOFIA, ROAD);
   addLink(g, SARAJEVO, ST_JOSEPH_AND_ST_MARYS, ROAD);
   addLink(g, SARAJEVO, VALONA, ROAD);
   addLink(g, SARAJEVO, ZAGREB, ROAD);
   addLink(g, SOFIA, VALONA, ROAD);
   addLink(g, SOFIA, VARNA, ROAD);
   addLink(g, STRASBOURG, ZURICH, ROAD);
   addLink(g, ST_JOSEPH_AND_ST_MARYS, SZEGED, ROAD);
   addLink(g, ST_JOSEPH_AND_ST_MARYS, ZAGREB, ROAD);
   addLink(g, SZEGED, ZAGREB, ROAD);
   addLink(g, VIENNA, ZAGREB, ROAD);

   //### RAIL Connections ###

   addLink(g, ALICANTE, BARCELONA, RAIL);
   addLink(g, ALICANTE, MADRID, RAIL);
   addLink(g, BARCELONA, SARAGOSSA, RAIL);
   addLink(g, BARI, NAPLES, RAIL);
   addLink(g, BELGRADE, SOFIA, RAIL);
   addLink(g, BELGRADE, SZEGED, RAIL);
   addLink(g, BERLIN, HAMBURG, RAIL);
   addLink(g, BERLIN, LEIPZIG, RAIL);
   addLink(g, BERLIN, PRAGUE, RAIL);
   addLink(g, BORDEAUX, PARIS, RAIL);
   addLink(g, BORDEAUX, SARAGOSSA, RAIL);
   addLink(g, BRUSSELS, COLOGNE, RAIL);
   addLink(g, BRUSSELS, PARIS, RAIL);
   addLink(g, BUCHAREST, CONSTANTA, RAIL);
   addLink(g, BUCHAREST, GALATZ, RAIL);
   addLink(g, BUCHAREST, SZEGED, RAIL);
   addLink(g, BUDAPEST, SZEGED, RAIL);
   addLink(g, BUDAPEST, VIENNA, RAIL);
   addLink(g, COLOGNE, FRANKFURT, RAIL);
   addLink(g, EDINBURGH, MANCHESTER, RAIL);
   addLink(g, FLORENCE, MILAN, RAIL);
   addLink(g, FLORENCE, ROME, RAIL);
   addLink(g, FRANKFURT, LEIPZIG, RAIL);
   addLink(g, FRANKFURT, STRASBOURG, RAIL);
   addLink(g, GENEVA, MILAN, RAIL);
   addLink(g, GENOA, MILAN, RAIL);
   addLink(g, LEIPZIG, NUREMBURG, RAIL);
   addLink(g, LE_HAVRE, PARIS, RAIL);
   addLink(g, LISBON, MADRID, RAIL);
   addLink(g, LIVERPOOL, MANCHESTER, RAIL);
   addLink(g, LONDON, MANCHESTER, RAIL);
   addLink(g, LONDON, SWANSEA, RAIL);
   addLink(g, MADRID, SANTANDER, RAIL);
   addLink(g, MADRID, SARAGOSSA, RAIL);
   addLink(g, MARSEILLES, PARIS, RAIL);
   addLink(g, MILAN, ZURICH, RAIL);
   addLink(g, MUNICH, NUREMBURG, RAIL);
   addLink(g, NAPLES, ROME, RAIL);
   addLink(g, PRAGUE, VIENNA, RAIL);
   addLink(g, SALONICA, SOFIA, RAIL);
   addLink(g, SOFIA, VARNA, RAIL);
   addLink(g, STRASBOURG, ZURICH, RAIL);
   addLink(g, VENICE, VIENNA, RAIL);

   //### BOAT Connections ###

   addLink(g, ADRIATIC_SEA, BARI, BOAT);
   addLink(g, ADRIATIC_SEA, IONIAN_SEA, BOAT);
   addLink(g, ADRIATIC_SEA, VENICE, BOAT);
   addLink(g, ALICANTE, MEDITERRANEAN_SEA, BOAT);
   addLink(g, AMSTERDAM, NORTH_SEA, BOAT);
   addLink(g, ATHENS, IONIAN_SEA, BOAT);
   addLink(g, ATLANTIC_OCEAN, BAY_OF_BISCAY, BOAT);
   addLink(g, ATLANTIC_OCEAN, CADIZ, BOAT);
   addLink(g, ATLANTIC_OCEAN, ENGLISH_CHANNEL, BOAT);
   addLink(g, ATLANTIC_OCEAN, GALWAY, BOAT);
   addLink(g, ATLANTIC_OCEAN, IRISH_SEA, BOAT);
   addLink(g, ATLANTIC_OCEAN, LISBON, BOAT);
   addLink(g, ATLANTIC_OCEAN, MEDITERRANEAN_SEA, BOAT);
   addLink(g, ATLANTIC_OCEAN, NORTH_SEA, BOAT);
   addLink(g, BARCELONA, MEDITERRANEAN_SEA, BOAT);
   addLink(g, BAY_OF_BISCAY, BORDEAUX, BOAT);
   addLink(g, BAY_OF_BISCAY, NANTES, BOAT);
   addLink(g, BAY_OF_BISCAY, SANTANDER, BOAT);
   addLink(g, BLACK_SEA, CONSTANTA, BOAT);
   addLink(g, BLACK_SEA, IONIAN_SEA, BOAT);
   addLink(g, BLACK_SEA, VARNA, BOAT);
   addLink(g, CAGLIARI, MEDITERRANEAN_SEA, BOAT);
   addLink(g, CAGLIARI, TYRRHENIAN_SEA, BOAT);
   addLink(g, DUBLIN, IRISH_SEA, BOAT);
   addLink(g, EDINBURGH, NORTH_SEA, BOAT);
   addLink(g, ENGLISH_CHANNEL, LE_HAVRE, BOAT);
   addLink(g, ENGLISH_CHANNEL, LONDON, BOAT);
   addLink(g, ENGLISH_CHANNEL, NORTH_SEA, BOAT);
   addLink(g, ENGLISH_CHANNEL, PLYMOUTH, BOAT);
   addLink(g, GENOA, TYRRHENIAN_SEA, BOAT);
   addLink(g, HAMBURG, NORTH_SEA, BOAT);
   addLink(g, IONIAN_SEA, SALONICA, BOAT);
   addLink(g, IONIAN_SEA, TYRRHENIAN_SEA, BOAT);
   addLink(g, IONIAN_SEA, VALONA, BOAT);
   addLink(g, IRISH_SEA, LIVERPOOL, BOAT);
   addLink(g, IRISH_SEA, SWANSEA, BOAT);
   addLink(g, MARSEILLES, MEDITERRANEAN_SEA, BOAT);
   addLink(g, MEDITERRANEAN_SEA, TYRRHENIAN_SEA, BOAT);
   addLink(g, NAPLES, TYRRHENIAN_SEA, BOAT);
   addLink(g, ROME, TYRRHENIAN_SEA, BOAT);
}

//from Map.c -------------------------------------------------------------------------------------------




#define ROUND_SIZE 40
#define TURN_SIZE 8
     
struct gameView {
    Round          currRound;
    PlayerID       currPlayer;
    int            currScore;
    int            currHealth[NUM_PLAYERS];
    char           *currPastPlays;
    PlayerMessage  *currMessages;
};

int currScore(char* pastPlays);
int currHealth(char *pastPlays, PlayerID currPlayer);

// Creates a new GameView to summarise the current state of the game
GameView newGameView(char *pastPlays, PlayerMessage messages[])
{
    GameView gameView                           = malloc(sizeof(struct gameView));
    int temp = strlen(pastPlays) + 1;
    gameView->currRound                         = temp / ROUND_SIZE;
    gameView->currPlayer                        = ((temp) % ROUND_SIZE) / TURN_SIZE;
    gameView->currScore                         = currScore(pastPlays);
    gameView->currHealth[PLAYER_LORD_GODALMING] = currHealth(pastPlays, PLAYER_LORD_GODALMING);
    gameView->currHealth[PLAYER_DR_SEWARD]      = currHealth(pastPlays, PLAYER_DR_SEWARD);
    gameView->currHealth[PLAYER_VAN_HELSING]    = currHealth(pastPlays, PLAYER_VAN_HELSING);
    gameView->currHealth[PLAYER_MINA_HARKER]    = currHealth(pastPlays, PLAYER_MINA_HARKER);
    gameView->currHealth[PLAYER_DRACULA]        = currHealth(pastPlays, PLAYER_DRACULA);
    gameView->currPastPlays                     = pastPlays;
    gameView->currMessages                      = messages;

    return gameView;
}

//// Functions to make newGameView

// Get the current score from the pastPlays string
int currScore(char* pastPlays)
{
    int i, j, k;
    int currScore           = GAME_START_SCORE;
    int currHealth          = GAME_START_HUNTER_LIFE_POINTS;
    LocationID prevLocation;
    LocationID currLocation;
    char currLocationStr[2];
    char *LocationPtr;

    // Loop through Dracula's plays
    for(i = PLAYER_DRACULA * TURN_SIZE; i < strlen(pastPlays); i += ROUND_SIZE)
    {
        // Decrease by 1 each time Dracula finishes a turn
        currScore -= SCORE_LOSS_DRACULA_TURN;

        // Decreases by 13 each time a Vampire matures (i.e falls off the trail)
        j = i + 6;
        if (pastPlays[j] == 'V')
            currScore -= SCORE_LOSS_VAMPIRE_MATURES;
    }

    // Decrease by 6 each time a hunter is transported to the hospital
    for(i = 0; i < NUM_PLAYERS - 2; i++)
    {
        for(j = i * TURN_SIZE; j < strlen(pastPlays); j += ROUND_SIZE)
        {
            // Gain 3 life points when resting in a city
            LocationPtr = pastPlays + j + 1;
            strncpy(currLocationStr, LocationPtr, 2);
            currLocation = abbrevToID(currLocationStr);

            if(currLocation == prevLocation)
                currHealth += LIFE_GAIN_REST;

            prevLocation = currLocation;

            // Loop through the 4 encounter characters
                // Lose 2 life points when a trap in encounted
                // Lose 4 life points when encounnting dracula
            for(k = j + 3; k < j + TURN_SIZE - 1; k++)
            {
                if(pastPlays[k] == 'T')
                {
                    currHealth -= LIFE_LOSS_TRAP_ENCOUNTER;
                }

                else if(pastPlays[k] == 'D')
                {
                    currHealth -= LIFE_LOSS_DRACULA_ENCOUNTER;
                }
            }

            // Adjust for max health for hunters of 9
            if(currHealth > GAME_START_HUNTER_LIFE_POINTS)
                    currHealth = GAME_START_HUNTER_LIFE_POINTS;

            // Hunters go to hospital if their health gets to 0 or less
            if(currHealth <= 0)
                currScore -= SCORE_LOSS_HUNTER_HOSPITAL;
        }
    }

    return currScore;
}

// Get the current health for a given player from the pastPlays string
int currHealth(char *pastPlays, PlayerID currPlayer)
{
    int currHealth;
    int i, j, k;
    LocationID prevLocation;
    LocationID currLocation;
    char currLocationStr[2];
    char * LocationPtr;
    
    // Health if player is a Hunter
    if(currPlayer < PLAYER_DRACULA)
    {
        currHealth = GAME_START_HUNTER_LIFE_POINTS;
        for(i = currPlayer * TURN_SIZE; i < strlen(pastPlays); i += ROUND_SIZE)
        {
            // Gain 3 life points when resting in a city
            LocationPtr = pastPlays + i + 1;
            strncpy(currLocationStr, LocationPtr, 2);
            currLocation = abbrevToID(currLocationStr);

            if(currLocation == prevLocation)
                currHealth += LIFE_GAIN_REST;

            prevLocation = currLocation;

            // Loop through the 4 encounter characters
                // Lose 2 life points when a trap in encounted
                // Lose 4 life points when encounnting dracula
            for(j = i + 3; j < i + TURN_SIZE; j++)
            {
                if(pastPlays[j] == 'T')
                {
                    currHealth -= LIFE_LOSS_TRAP_ENCOUNTER;
                }

                else if(pastPlays[j] == 'D')
                {
                    currHealth -= LIFE_LOSS_DRACULA_ENCOUNTER;
                }
            }

            // Max health for hunters is 9
            if(currHealth > GAME_START_HUNTER_LIFE_POINTS)
                    currHealth = GAME_START_HUNTER_LIFE_POINTS;
        }
    } 

    // Health for Dracula
    else if(currPlayer == PLAYER_DRACULA)
    {
        currHealth = GAME_START_BLOOD_POINTS;
        for(i = PLAYER_DRACULA * TURN_SIZE; i < strlen(pastPlays); i += ROUND_SIZE)
        {
            LocationPtr = pastPlays + i + 1;
            strncpy(currLocationStr, LocationPtr, 2);
            currLocation = abbrevToID(currLocationStr);

            // Double Back sea loss
            if(currLocationStr[0] == 'D' && currLocationStr[1] != 'U')
            {
                int distance;
                switch(currLocationStr[1])
                {
                    case '1': distance = 1; break;
                    case '2': distance = 2; break;
                    case '3': distance = 3; break;
                    case '4': distance = 4; break;
                    case '5': distance = 5; break;
                }
                currLocationStr[0] = pastPlays[i-ROUND_SIZE*distance+1];
                currLocationStr[1] = pastPlays[i-ROUND_SIZE*distance+2];
            }

            // Sea loss
            if (currLocationStr[0] == 'S')
            {
                currHealth -= LIFE_LOSS_SEA;
            }
            else if((currLocationStr[0] == 'A' && currLocationStr[1] == 'S') ||
                    (currLocationStr[0] == 'A' && currLocationStr[1] == 'O') ||
                    (currLocationStr[0] == 'B' && currLocationStr[1] == 'B') ||
                    (currLocationStr[0] == 'B' && currLocationStr[1] == 'S') ||
                    (currLocationStr[0] == 'E' && currLocationStr[1] == 'C') ||
                    (currLocationStr[0] == 'I' && currLocationStr[1] == 'O') ||
                    (currLocationStr[0] == 'I' && currLocationStr[1] == 'R') ||
                    (currLocationStr[0] == 'M' && currLocationStr[1] == 'S') ||
                    (currLocationStr[0] == 'N' && currLocationStr[1] == 'S') ||
                    (currLocationStr[0] == 'T' && currLocationStr[1] == 'S'))
            {
                currHealth -= LIFE_LOSS_SEA;                
            }

            // Castle gain
            if(currLocation == CASTLE_DRACULA)
                currHealth += LIFE_GAIN_CASTLE_DRACULA;

            prevLocation = currLocation;
        }

        for(k = 0; k < 4; k++) // loop through each of the hunters
        {

            for(i = k * TURN_SIZE; i < strlen(pastPlays); i += ROUND_SIZE) //loop through each turn
            {

                // Loop through the 4 encounter characters
                // Lose 10 blood points when a hunter encounters dracula
                for(j = i + 3; j < i + TURN_SIZE; j++)
                {

                    if(pastPlays[j] == 'D')
                    {
                        currHealth -= LIFE_LOSS_HUNTER_ENCOUNTER;
                    }
                }
            }
        }
    } 

    // Invalid currPlayer
    else 
    {
        currHealth = -1;
    }

    return currHealth;
}
    
// Frees all memory previously allocated for the GameView toBeDeleted
void disposeGameView(GameView toBeDeleted)
{
    /*free((void *)toBeDeleted->currHealth);
    free((void *)toBeDeleted->currPastPlays);
    free((void *)toBeDeleted->currMessages);*/

    free(toBeDeleted);
}

//// Functions to return simple information about the current state of the game

// Get the current round
Round getRound(GameView currentView)
{
    return currentView->currRound;
}

// Get the id of current player - ie whose turn is it?
PlayerID getCurrentPlayer(GameView currentView)
{
    return currentView->currPlayer;
}

// Get the current score
int getScore(GameView currentView)
{
    return currentView->currScore;
}

// Get the current health points for a given player
int getHealth(GameView currentView, PlayerID player)
{
    return currentView->currHealth[player];
}

// Get the current location id of a given player
LocationID getLocation(GameView currentView, PlayerID player)
{
    /*
    int index;
    char currLocationStr[2];
    char *LocationPtr;
    index                   = (ROUND_SIZE * currentView->currRound) +
                              (TURN_SIZE * player);

    LocationPtr = currentView->currPastPlays + index + 1;
    strncpy(currLocationStr, LocationPtr, 2);

    return abbrevToID(currLocationStr);
    */

    char currLocationStr[2];
    int index;

    //if given player has already made a move this round
    if (player < currentView->currPlayer)
    {
        index = (ROUND_SIZE*currentView->currRound) + (TURN_SIZE*player);
        currLocationStr[0] = currentView->currPastPlays[index+1];
        currLocationStr[1] = currentView->currPastPlays[index+2];
    }
    else //given player has not made a move this round, check previous round
    {
        if (currentView->currRound == 0) //has not made a move yet
        {
            return UNKNOWN_LOCATION;
        }

        index = (ROUND_SIZE*(currentView->currRound-1)) + (TURN_SIZE*player);
        currLocationStr[0] = currentView->currPastPlays[index+1];
        currLocationStr[1] = currentView->currPastPlays[index+2];
    }

    if (player == PLAYER_DRACULA)
    {
        //player is Dracula

        if (strcmp(currLocationStr, "HI") == 0)
        {
            //hide
            if (currentView->currPlayer == PLAYER_DRACULA)
            {
                //dracula knows his position
                currLocationStr[0] = currentView->currPastPlays[index-ROUND_SIZE+1];
                currLocationStr[1] = currentView->currPastPlays[index-ROUND_SIZE+2];    
            }
            else
            {
                //hunters do not know dracula's position
                return HIDE;
            }
        }
        else if (currLocationStr[0] == 'D' && currLocationStr[1] != 'U')
        {
            //double back
            if (currentView->currPlayer == PLAYER_DRACULA)
            {
                //dracula should know his position
                int distance;
                distance = currLocationStr[1] = currentView->currPastPlays[index+2];
                currLocationStr[0] = currentView->currPastPlays[index-ROUND_SIZE*distance+1];
                currLocationStr[1] = currentView->currPastPlays[index-ROUND_SIZE*distance+2];    
            }
            else
            {
                //hunters do not know dracula's position
                switch(currLocationStr[1])
                {
                    case '1': return DOUBLE_BACK_1; break;
                    case '2': return DOUBLE_BACK_2; break;
                    case '3': return DOUBLE_BACK_3; break;
                    case '4': return DOUBLE_BACK_4; break;
                    case '5': return DOUBLE_BACK_5; break;
                }
            }
        }
        else if (strcmp(currLocationStr, "TP") == 0)
        {
            currLocationStr[0] = 'C';
            currLocationStr[1] = 'D';
        }
        else if (currLocationStr[1] == '?')
        {
            //unknown location
            switch (currLocationStr[0])
            {
                case 'C': return CITY_UNKNOWN; break;
                case 'S': return SEA_UNKNOWN; break;

            }
        }        
    }
    return abbrevToID(currLocationStr);
}

//// Functions that return information about the history of the game

// Fills the trail array with the location IDs of the last 6 turns
void getHistory(GameView currentView, PlayerID player,
                LocationID trail[TRAIL_SIZE])
{
    int index, i;
    char currLocationStr[2];
    char *LocationPtr;

    int hasMoved = 0;

    //if given player has already made a move this round
    if (player < currentView->currPlayer)
    {
        hasMoved = 1;
        index                   = (ROUND_SIZE * getRound(currentView)) +
                                  (TURN_SIZE * player);
    }
    else //given player has not made a move this round, check previous round
    {
        if (currentView->currRound != 0) //not the first round
        {
            hasMoved = 1;
            index               = (ROUND_SIZE * (getRound(currentView) - 1)) +
                                  (TURN_SIZE * player);
        }
    }

    for(i = 0; i < TRAIL_SIZE; i++)
    {
        trail[i] = UNKNOWN_LOCATION;
    }

    if(hasMoved)
    {
        //fill the 6 elements of the array and stop if index goes below zero
        for(i = 0; i < 6 && index >= 0; i++, index -= ROUND_SIZE) 
        {
            LocationPtr = currentView->currPastPlays + index + 1;
            strncpy(currLocationStr, LocationPtr, 2);
            if(currLocationStr[0] == 'D' && currLocationStr[1] != 'U')
            {
                //double back
                if (currentView->currPlayer == PLAYER_DRACULA)
                {
                    //dracula should know his position
                    int distance;
                    distance = currLocationStr[1] = currentView->currPastPlays[index+2];
                    currLocationStr[0] = currentView->currPastPlays[index-ROUND_SIZE*distance+1];
                    currLocationStr[1] = currentView->currPastPlays[index-ROUND_SIZE*distance+2];    
                }
                else
                {
                    switch(currLocationStr[1])
                    {
                        case '1': trail[i] = DOUBLE_BACK_1; break;
                        case '2': trail[i] = DOUBLE_BACK_2; break;
                        case '3': trail[i] = DOUBLE_BACK_3; break;
                        case '4': trail[i] = DOUBLE_BACK_4; break;
                        case '5': trail[i] = DOUBLE_BACK_5; break;
                    }
                }
            }
            else if(currLocationStr[0] == 'T' && currLocationStr[1] == 'P')
            {
                trail[i] = TELEPORT;
            }
            else if(currLocationStr[0] == 'H' && currLocationStr[1] == 'I')
            {
                trail[i] = HIDE;
            }
            else if(currLocationStr[0] == 'S' && currLocationStr[1] == '?')
            {
                trail[i] = SEA_UNKNOWN;
            }
            else if(currLocationStr[0] == 'C' && currLocationStr[1] == '?')
            {
                trail[i] = CITY_UNKNOWN;
            }        
            else
            {    
                trail[i] = abbrevToID(currLocationStr);
            }
        }
    }
}

//// Functions that query the map to find information about connectivity

// Returns an array of LocationIDs for all directly connected locations

LocationID *connectedLocations(GameView currentView, int *numLocations,
                               LocationID from, PlayerID player, Round round,
                               int road, int rail, int sea)
{
	LocationID *connectedLocations = malloc(*numLocations * sizeof(LocationID));
    connectedLocations[0] = from;
    
    Map currMap = newMap();
    int i, j, k, counter, roundSum, currRound, railDistance;
    int isInArray;

    // Get the list of connections
    VList fromConnections;

    // Check if player is legit
    if(player <= PLAYER_DRACULA)
    {
    	
    	if(player == PLAYER_DRACULA) rail = FALSE;
    	

    	// Fill the array
    	for(fromConnections = currMap->connections[from], i = 1;
    		fromConnections->next != NULL;
    		fromConnections = fromConnections->next)
    	{
    		if(fromConnections->type == ROAD && road == TRUE)
    		{
    			connectedLocations[i] = fromConnections->v;
    			i++;
    		}





    		else if(fromConnections->type == RAIL && rail == TRUE)
    		{
    			// Get the distance the player can travel by rail this turn
    			for(roundSum = 0; currRound > 0; currRound--, roundSum += currRound);
    			railDistance = roundSum % 4;

    			if(railDistance == 1)
    			{
    				connectedLocations[i] = fromConnections->v;
    			}


    			else if(railDistance >= 2)
    			{
    				VList railFirstConnections;
    				for(railFirstConnections = currMap->connections[fromConnections->v], j = 1;
    					railFirstConnections->next != NULL;
    					railFirstConnections = railFirstConnections->next)
    				{
    					// Find rail connections
    					if(railFirstConnections->type == RAIL)
    					{
    						// Check if its not in array
    						isInArray = FALSE;
    						for(counter = 0; counter < *numLocations; counter++)
    						{
    							if(connectedLocations[counter] == railFirstConnections->v) isInArray = TRUE;
    						}

    						if(isInArray == FALSE)
    						{
    							connectedLocations[i + j] = railFirstConnections->v;
    							j++;
    						}

    						if(railDistance == 3)
    						{
			    				VList railSecondConnections;
			    				for(railSecondConnections = currMap->connections[fromConnections->v], k = 1;
			    					railSecondConnections->next != NULL;
			    					railSecondConnections = railSecondConnections->next)
			    				{
			    					// Find rail connections
			    					if(railSecondConnections->type == RAIL)
			    					{
			    						// Check if its not in array
			    						isInArray = FALSE;
			    						for(counter = 0; counter < *numLocations; counter++)
			    						{
			    							if(connectedLocations[counter] == railSecondConnections->v) isInArray = TRUE;
			    						}

			    						if(isInArray == FALSE)
			    						{
			    							connectedLocations[i + j + k] = railSecondConnections->v;
			    							k++;
			    						}
			    					}
			    				}

			    				j = j + k;
			    			}
    					}
    				}

    				i = i + j;
    			}
    		}




    		else if(fromConnections->type == SEA && sea == TRUE)
    		{
    			connectedLocations[i] = fromConnections->v;
    		}
    	}

    	disposeMap(currMap);
    }
        
    return connectedLocations;
}